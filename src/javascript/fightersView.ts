import View from './view';
import FighterView from './fighterView';
import {Fighter, IFighter} from './models/fighter';
import {fighterService} from './services/fightersService';
import FightView from './fightView';
import { start } from 'repl';
import { filter } from 'minimatch';

class FightersView extends View {

  private clicks_counter: number = 0;
  private firstFighter: IFighter;
  private secondFighter: IFighter;

  handleInfo: (event: Event, fighterDetails: IFighter) => Promise<void>;
  handleChoose: (event: Event, fighterDetails: IFighter) => Promise<void>;
  handleSave: (event: Event, fighterDetails: IFighter) => Promise<void>;

  constructor(fighters: IFighter[]) {
    super();
    this.handleChoose = this.handleCheckClick.bind(this);
    this.handleInfo = this.handleInfoClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map<number, IFighter>();

  public createFighters(fighters: IFighter[]) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleInfo, this.handleChoose);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: ['fighters'] });
    this.element.append(...fighterElements);
  }

  public async handleCheckClick(event: Event, fighter: IFighter): Promise<void> {

    let hasFighter = this.fightersDetailsMap.has(fighter._id)
    if (!hasFighter) {
      let fighterDetails: Fighter = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }

    this.clicks_counter++;
    (this.clicks_counter == 1) ? this.firstFighter = fighter : this.secondFighter = fighter;

    let fighterView = document.getElementById(fighter.name);
    fighterView.style.backgroundColor = 'lightgray';
    fighterView.style.border = '1px solid yellow';
    if (this.clicks_counter == 2 && this.firstFighter._id != this.secondFighter._id) {
      let startButton = this.createElement({tagName: 'button', className: ['btn', 'btn-info'], attributes: { "id":"start-button", "type":"button"}});
      startButton.innerText = "Start fight";
      document.getElementById('root').append(startButton);
      this.handleStartButtonClick();
    }

    if (this.clicks_counter > 2) {
      this.firstFighter = null;
      this.secondFighter = null;
      let elements = document.getElementsByClassName('fighter');
      Array.from(elements).forEach(item => {
        (<HTMLElement>item).style.backgroundColor = "white";
        (<HTMLElement>item).style.border = "none";
      });
      this.clicks_counter = 0;
      document.getElementById('start-button').remove();
    }
  }

  public async handleInfoClick(event: Event, fighter: IFighter): Promise<void> {

    let hasFighter = this.fightersDetailsMap.has(fighter._id)

    if (!hasFighter) {
      let fighterDetails: Fighter = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }
    document.getElementById('fighter-name').innerText = this.fightersDetailsMap.get(fighter._id).name;
    (<HTMLInputElement>document.getElementById('health')).value = this.fightersDetailsMap.get(fighter._id).health.toString();
    (<HTMLInputElement>document.getElementById('power')).value = this.fightersDetailsMap.get(fighter._id).attack.toString();
    (<HTMLInputElement>document.getElementById('defense')).value = this.fightersDetailsMap.get(fighter._id).defense.toString();
    document.getElementById('save-button').addEventListener('click', event => this.handleSaveButtonClick(event, fighter), false);

  }

  public handleStartButtonClick(): void {
    document.getElementById('start-button').addEventListener('click', () => {
      document.getElementById('root').remove();
      new FightView(this.fightersDetailsMap.get(this.firstFighter._id), this.fightersDetailsMap.get(this.secondFighter._id));
    })
  }

  public handleSaveButtonClick(event: Event, fighter: IFighter): void {

      let health: number = Number((<HTMLInputElement>document.getElementById('health')).value);
      let attack: number = Number((<HTMLInputElement>document.getElementById('power')).value);
      let defense: number = Number((<HTMLInputElement>document.getElementById('defense')).value);
    
      if((health > 0 && health <= 100) && (attack > 0 && attack <= 10) && (defense > 0 && defense <= 10)) {
          this.fightersDetailsMap.get(fighter._id).health = health;
          this.fightersDetailsMap.get(fighter._id).attack = attack;
          this.fightersDetailsMap.get(fighter._id).defense = defense;
          alert(`The information was successfully updated!`);
      }
      else {
        alert('Please, input corrent values(0-100 for health, 0-10 for power and defense')
      }
    }
};



export default FightersView;