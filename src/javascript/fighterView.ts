import View from './view';
import {Fighter, IFighter} from './models/fighter';


class FighterView extends View {
  constructor(fighter: Fighter, handleInfoClick: (event: Event, fighter: IFighter) => void = null, handleChooseClick: (event: Event, fighter: IFighter) => void = null) {
    super();
    this.createFighter(fighter, handleInfoClick, handleChooseClick);
  }

  private createFighter(fighter: IFighter, handleInfoClick: (event: Event, fighter: IFighter) => void, handleChooseClick: (event: Event, fighter: IFighter) => void) {
    const { name, source } = fighter;
    const nameElement: HTMLElement = this.createName(name);
    const imageElement: HTMLElement = this.createImage(source);
    const iconsContainer: HTMLElement = this.createElement({tagName: 'div', className: ['iconsContainer']});
    const infoIconElement: HTMLElement = this.createIcons(['fas', 'fa-info-circle'], {"data-toggle": "modal", "data-target": "#fighterDetailsWindow"});
    const chooseIconElement: HTMLElement = this.createIcons(['fas', 'fa-check-circle']);

    const attributes = { "id": name};

    iconsContainer.append(infoIconElement, chooseIconElement);

    this.element = this.createElement({ tagName: 'div', className: ['fighter'], attributes });
    infoIconElement.addEventListener('click', event => handleInfoClick(event, fighter), false);
    chooseIconElement.addEventListener('click', event => handleChooseClick(event, fighter), false);
    this.element.append(imageElement, nameElement, iconsContainer);
  }

  public createIcons(names: string[], attributes = {}): HTMLElement {
    const iconElement: HTMLElement = this.createElement({tagName: 'i', className: names, attributes: attributes});
    
    return iconElement;
  }

  public createName(name: string): HTMLElement {
    const nameElement: HTMLElement = this.createElement({ tagName: 'span', className: ['name'] });
    nameElement.innerText = name;

    return nameElement;
  }

  public createImage(source: string): HTMLElement {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: ['fighter-image'],
      attributes
    });

    return imgElement;
  }
}

export default FighterView;