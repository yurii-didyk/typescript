import View from './view';
import FighterView from './fighterView';
import {fighterService} from './services/fightersService';
import { Fighter, IFighter} from './models/fighter';
import App from './app';
import { setTimeout } from 'timers';

class FightView extends View {

    private firstFighterView: FighterView;
    private secondFighterView: FighterView;
    private secondTotal: number;
    private firstTotal:number;

    constructor(firstFighter: IFighter, secondFighter: IFighter) {
        super();
        
        this.createFight(firstFighter, secondFighter);
        this.handleFirstFighterAttack(firstFighter, secondFighter);
        this.handleSecondFighterAttack(firstFighter, secondFighter);
    }

    public createFight(firstFighter: IFighter, secondFighter: IFighter): void {
        this.firstFighterView = new FighterView(firstFighter);
        this.secondFighterView = new FighterView(secondFighter);

        this.element = this.createElement({tagName: 'div', className: ['game-wrapper']});

        let leftFighter = this.createElement({tagName: 'div', className: ['first-fighter']});
        let rightFighter = this.createElement({tagName: 'div', className: ['second-fighter']});

        let leftHealth = this.createElement({tagName: 'div', className:['health-bar'], attributes: {"id":"left", "data-total": `${firstFighter.health}`, "data-value": `${firstFighter.health}`}})

        let rightHealth = this.createElement({tagName: 'div', className:['health-bar'], attributes: {"id":"right", "data-total": `${secondFighter.health}`, "data-value": `${secondFighter.health}`}})
        let barLeft = this.createElement({tagName: 'div', className: ['bar'], attributes: {"id":"leftBar"}});
        let hitLeft = this.createElement({tagName: 'div', className: ['hit'], attributes: {"id": "leftHit"}});
        let barRight = this.createElement({tagName: 'div', className: ['bar'], attributes: {"id":"rightBar"}});
        let hitRight = this.createElement({tagName: 'div', className: ['hit'], attributes: {"id": "rightHit"}});
        barLeft.append(hitLeft);
        barRight.append(hitRight);
        leftHealth.append(barLeft);
        rightHealth.append(barRight);

        leftFighter.append(this.firstFighterView.element, leftHealth);
        rightFighter.append(this.secondFighterView.element, rightHealth);

        this.element.append(leftFighter, rightFighter);

        this.firstTotal = firstFighter.health;
        this.secondTotal = secondFighter.health;

        let info = this.createElement({tagName: 'span', className: ['info']});
        info.innerText = 'Click on character to hit...';
        


        document.getElementsByTagName('body')[0].append(this.element, info);
       
    }

    

    public handleFirstFighterAttack(firstFighter: IFighter, secondFighter: IFighter) {
        this.firstFighterView.element.addEventListener('click', event => this.firstFighterAttack(event, firstFighter, secondFighter));
        
    }
    public handleSecondFighterAttack(firstFighter: IFighter, secondFighter: IFighter) {
        this.secondFighterView.element.addEventListener('click', event => this.secondFighterAttack(event, firstFighter, secondFighter));
        
    }

    public firstFighterAttack(event: Event, firstFighter: IFighter, secondFighter: IFighter): void {
        let damage = firstFighter.getHitPower() - secondFighter.getBlockPower();
        let total = secondFighter.health;
        if (damage > 0) {
            secondFighter.health -= damage;
            let rightHealthBar = document.getElementById('right');
            let rightBar = document.getElementById('rightBar');
            let rightHit = document.getElementById('rightHit');

            let barWidth = (secondFighter.health / this.secondTotal) * 100 ;
            let hitWidth = (damage / total) * 100 + "%";

            rightHit.style.width = hitWidth;
            rightHealthBar.setAttribute("data-value",`${secondFighter.health}`);

            setTimeout(() => {
                rightHit.style.width = '0';
                rightBar.style.width = barWidth + "%";
            }, 500);
        }
        if (secondFighter.health <= 0) {
            let label = this.createElement({tagName: "span", className:['winner-display']});
            label.innerText = (`Congatulations! ${firstFighter.name} won!`);
            document.getElementsByTagName('body')[0].append(label);
            setTimeout(() => {
                document.location.reload(true);
            }, 1000);
        }
    }

    public secondFighterAttack(event: Event, firstFighter: IFighter, secondFighter: IFighter): void {
        let damage = secondFighter.getHitPower() - firstFighter.getBlockPower();
        let total = firstFighter.health;
        if (damage > 0) {
            firstFighter.health -= damage;
            let leftHealthBar = document.getElementById('left');
            let leftBar = document.getElementById('leftBar');
            let leftHit = document.getElementById('leftHit');

            let barWidth = (firstFighter.health / this.firstTotal) * 100 ;
            let hitWidth = (damage / total) * 100 + "%";

            leftHit.style.width = hitWidth;
            leftHealthBar.setAttribute("data-value",`${secondFighter.health}`);

            setTimeout(() => {
                leftHit.style.width = '0';
                leftBar.style.width = barWidth + "%";
            }, 500);
        }
        if (firstFighter.health <= 0) {
            let label = this.createElement({tagName: "span", className:['winner-display']});
            label.innerText = (`Congatulations! ${secondFighter.name} won!`);
            document.getElementsByTagName('body')[0].append(label);
            
            setTimeout(() => {
                document.location.reload(true);
            }, 1000);
        }
    }
}

export default FightView;