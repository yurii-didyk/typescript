class View {

  public element?: HTMLElement;

  public createElement({ tagName, className = [''], attributes = {} }: { tagName: string, className: string[], attributes?: { [key: string]: string } }): HTMLElement {
    const element: HTMLElement = document.createElement(tagName);
    Array.from(className).forEach(item => {
      element.classList.add(item);
    });
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;
