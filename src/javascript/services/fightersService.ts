import { callApi } from '../helpers/apiHelper';
import { Fighter, IFighter } from '../models/fighter';
import { decode } from 'punycode';

class FighterService {

  public async getFighters(): Promise<IFighter[]> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  public async getFighterDetails(_id: number): Promise<IFighter> {

    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return new Fighter(JSON.parse(atob(apiResult.content)));
    } catch (error) {
      throw error;
    }
  }
}


export const fighterService = new FighterService();
